package org.acme.model;

import jakarta.validation.constraints.NotEmpty;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.util.Arrays;

@Schema(description = "A request to insert a new post")
public record PostRequest(
        @NotEmpty String title,
        @NotEmpty String content,
        String[] tags) {

    @Override
    public String toString() {
        return "PostRequest{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", tags=" + Arrays.toString(tags) +
                '}';
    }
}
