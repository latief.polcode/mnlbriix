package org.acme.service;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import org.acme.dto.PostDTO;
import org.acme.model.Post;
import org.acme.model.PostRequest;
import org.acme.model.Tag;
import org.acme.repository.PostRepository;
import org.acme.repository.TagRepository;
import org.jboss.logging.Logger;

import java.util.*;
import java.util.stream.Collectors;


@ApplicationScoped
public class BlogService {
	@Inject
	PostRepository postRepository;

	@Inject
	TagRepository tagRepository;

	private static final Logger LOGGER = Logger.getLogger(BlogService.class);

	public BlogService() {
	}

	@Transactional
	public Post performPost(PostRequest postRequest) {
		Post newPost = new Post(postRequest.title(), postRequest.content());
		Set<Tag> tags = new HashSet<>();
		for (String label : postRequest.tags()) {
			Tag tag = tagRepository.findByLabel(label)
					.orElseGet(() -> {
						Tag newTag = new Tag(label);
						tagRepository.persist(newTag); // Persist the new tag if it doesn't exist
						return newTag;
					});
			tags.add(tag);
		}
		newPost.setTags(tags);
		postRepository.persist(newPost);
		return newPost;
	}
	@Transactional
	public PostDTO getPostDTO(Long postId) {
		Post post = postRepository.findById(postId);
		if (post != null) {
			Set<String> tagLabels = post.getTags().stream()
					.map(Tag::getLabel)
					.collect(Collectors.toSet());
			return new PostDTO(post.getId(), post.getTitle(), post.getContent(), tagLabels);
		} else {
			throw new EntityNotFoundException("Post not found");
		}
	}

	@Transactional
	public void updatePost(Long id, PostRequest postRequest) {
		Post existingPost = postRepository.findById(id);
		if (existingPost != null) {
			existingPost.setTitle(postRequest.title());
			existingPost.setContent(postRequest.content());

			// Remove all current tags and add new ones
			existingPost.getTags().clear();
			for (String label : postRequest.tags()) {
				Tag tag = tagRepository.findByLabel(label)
						.orElseGet(() -> {
							Tag newTag = new Tag();
							newTag.setLabel(label);
							// Persist the new tag if it doesn't exist
							tagRepository.persist(newTag);
							return newTag;
						});
				existingPost.addTag(tag);
			}
		} else {
			throw new EntityNotFoundException("Post with id " + id + " not found");
		}
	}
}
