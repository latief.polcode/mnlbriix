package org.acme.dto;

import java.util.Set;

public class PostDTO {
    private Long id;
    private String title;
    private String content;
    private Set<String> tagLabels;

    public PostDTO(Long id, String title, String content, Set<String> tagLabels) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.tagLabels = tagLabels;
    }

    // Getters
    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public Set<String> getTagLabels() {
        return tagLabels;
    }

    // Setters
    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setTagLabels(Set<String> tagLabels) {
        this.tagLabels = tagLabels;
    }

    // toString() method for debugging purposes
    @Override
    public String toString() {
        return "PostDTO{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", tagLabels=" + tagLabels +
                '}';
    }
}
