package org.acme.rest;

import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Response;
import org.acme.dto.PostDTO;
import org.acme.model.Post;
import org.acme.model.PostRequest;
import org.acme.service.BlogService;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.ExampleObject;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.logging.Logger;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/api/posts")
@Produces(APPLICATION_JSON)
@Tag(name = "posting")
public class BlogResource {
	private static final Logger LOGGER = Logger.getLogger(BlogResource.class);
	private final BlogService service;

	public BlogResource(BlogService service) {
		this.service = service;
	}

	@GET
	@Path("/{id}")
	@Operation(summary = "Get a post by ID")
	@APIResponse(
			responseCode = "200",
			description = "The post",
			content = @Content(
					mediaType = APPLICATION_JSON,
					schema = @Schema(implementation = PostDTO.class)
			)
	)
	@APIResponse(
			responseCode = "404",
			description = "Post not found"
	)
	public Response getPost(
			@PathParam("id") Long id) {
		try {
			PostDTO postDTO = this.service.getPostDTO(id);
			return Response.ok(postDTO).build();
		} catch (EntityNotFoundException e) {
			return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
		}
	}

	@POST
	@Consumes(APPLICATION_JSON)
	@Operation(summary = "Insert a post")
	@APIResponse(
		responseCode = "200",
		description = "The post",
    content = @Content(
      mediaType = APPLICATION_JSON,
      schema = @Schema(implementation = Post.class),
      examples = @ExampleObject(name = "post", value = "{\"id\": \"653bea9d188984908cd12429\", \"title\": \"ABC of the Title\", \"content\": \"Once upon a time, there are ...\", \"tags\": [\\\"this\\\", \\\"suppose\\\", \\\"to\\\", \\\"be\\\", \\\"array\\\", \\\"string\\\"]}")
    )
	)
	@APIResponse(
		responseCode = "400",
		description = "Invalid post passed in (or no request body found)"
	)
	public Response perform(
    @RequestBody(
      name = "post_request",
      required = true,
      content = @Content(
        mediaType = APPLICATION_JSON,
        schema = @Schema(implementation = PostRequest.class),
        examples = @ExampleObject(name = "posting", value = "{\"id\": \"653bea9d188984908cd12429\", \"title\": \"ABC of the Title\", \"content\": \"Once upon a time, there are ...\", \"tags\": [\\\"this\\\", \\\"suppose\\\", \\\"to\\\", \\\"be\\\", \\\"array\\\", \\\"string\\\"]}")
      )
    )
	@NotNull @Valid PostRequest postRequest) {
		try {
			LOGGER.info("Performing post_request: " + postRequest);
			this.service.performPost(postRequest);
			return Response.ok("Successfully Inserted").build();
		} catch (IllegalStateException e) {
			return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
		}
	}

	@PUT
	@Path("/{id}")
	@Consumes(APPLICATION_JSON)
	@Operation(summary = "Update a post")
	@APIResponse(
			responseCode = "200",
			description = "The updated post",
			content = @Content(
					mediaType = APPLICATION_JSON,
					schema = @Schema(implementation = Post.class),
					examples = @ExampleObject(name = "post", value = "{\"id\": \"653bea9d188984908cd12429\", \"title\": \"Updated Title\", \"content\": \"Updated content goes here...\", \"tags\": [\"updated\", \"tags\"]}")
			)
	)
	@APIResponse(
			responseCode = "400",
			description = "Invalid post passed in (or no request body found)"
	)
	@APIResponse(
			responseCode = "404",
			description = "Post not found"
	)
	public Response updatePost(
			@PathParam("id") Long id,
			@RequestBody(
					name = "post_update_request",
					required = true,
					content = @Content(
							mediaType = APPLICATION_JSON,
							schema = @Schema(implementation = PostRequest.class),
							examples = @ExampleObject(name = "postUpdate", value = "{\"title\": \"Updated Title\", \"content\": \"Updated content goes here...\", \"tags\": [\"updated\", \"tags\"]}")
					)
			)
			@NotNull @Valid PostRequest postRequest) {
		try {
			this.service.updatePost(id, postRequest);
			return Response.ok("Successfully update").build();
		} catch (IllegalStateException e) {
			return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
		}
	}
}
