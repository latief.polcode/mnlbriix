package org.acme.service;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.acme.dto.PostDTO;
import org.acme.model.Post;
import org.acme.model.PostRequest;
import org.acme.repository.PostRepository;
import org.acme.repository.TagRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
public class BlogServiceTest {

    @Inject
    BlogService blogService;

    @Inject
    PostRepository postRepository;

    @Inject
    TagRepository tagRepository;

    // Prepares the test environment before each test
    @BeforeEach
    public void setUp() {
        // Clean up the database or set up initial state if required
    }

    @Test
    @Transactional
    public void testPerformPost() {
        // Given
        PostRequest postRequest = new PostRequest("Test Post", "Test Content", new String[]{"Tag1", "Tag2"});

        // When
        Post result = blogService.performPost(postRequest);

        // Then
        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals("Test Post", result.getTitle());
        assertEquals("Test Content", result.getContent());
        assertFalse(result.getTags().isEmpty());
    }

    @Test
    @Transactional
    public void testGetPostDTO() {
        // Set up a new Post in the database
        Post post = new Post("Test Post for DTO", "Test Content for DTO");
        postRepository.persistAndFlush(post);

        // When
        PostDTO postDTO = blogService.getPostDTO(post.getId());

        // Then
        assertNotNull(postDTO);
        assertEquals(post.getTitle(), postDTO.getTitle());
        assertEquals(post.getContent(), postDTO.getContent());
    }

    @Test
    @Transactional
    public void testUpdatePost() {
        // Set up a new Post in the database
        Post existingPost = new Post("Original Title", "Original Content");
        postRepository.persistAndFlush(existingPost);

        // Given
        PostRequest updateRequest = new PostRequest("Updated Title", "Updated Content", new String[]{"Tag1", "Tag2"});

        // When
        blogService.updatePost(existingPost.getId(), updateRequest);

        // Then
        Post updatedPost = postRepository.findById(existingPost.getId());
        assertNotNull(updatedPost);
        assertEquals("Updated Title", updatedPost.getTitle());
        assertEquals("Updated Content", updatedPost.getContent());
    }
}
