package org.acme.rest;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import jakarta.transaction.Transactional;
import org.acme.model.PostRequest;
import org.junit.jupiter.api.Test;

import jakarta.ws.rs.core.Response;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

@QuarkusTest
public class BlogResourceTest {

    @Test
    public void testGetPostEndpoint() {
        long postId = 1; // This ID should exist in the database for the test to pass
        given()
                .pathParam("id", postId)
                .when().get("/api/posts/{id}")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .body("title", is(notNullValue()))
                .body("content", is(notNullValue()));
    }

    @Test
    public void testCreatePostEndpoint() {
        PostRequest postRequest = new PostRequest("New Post", "New post content", new String[]{"Z"});
        given()
                .contentType(ContentType.JSON)
                .body(postRequest)
                .when().post("/api/posts")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .body(is("Successfully Inserted"));
    }

    @Test
    public void testUpdatePostEndpoint() {
        long postId = 1; // This ID should exist and be updatable in the database for the test to pass
        PostRequest postRequest = new PostRequest("Updated Post", "Updated post content", new String[]{"Java", "Quarkus"});

        given()
                .contentType(ContentType.JSON)
                .pathParam("id", postId)
                .body(postRequest)
                .when().put("/api/posts/{id}")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .body(is("Successfully update"));
    }
}
