package org.acme.dto;

import org.junit.jupiter.api.Test;
import java.util.HashSet;
import java.util.Set;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class PostDTOTest {

    @Test
    void testPostDTOFields() {
        // Arrange
        Long id = 1L;
        String title = "Test Post";
        String content = "This is a test post";
        Set<String> tags = new HashSet<>();
        tags.add("Java");
        tags.add("Quarkus");

        // Act
        PostDTO postDTO = new PostDTO(id, title, content, tags);

        // Assert
        assertEquals(id, postDTO.getId());
        assertEquals(title, postDTO.getTitle());
        assertEquals(content, postDTO.getContent());
        assertEquals(tags, postDTO.getTagLabels());
    }

    @Test
    void testPostDTOToString() {
        // Arrange
        Long id = 1L;
        String title = "Test Post";
        String content = "This is a test post";
        Set<String> tags = new HashSet<>();
        tags.add("Java");
        PostDTO postDTO = new PostDTO(id, title, content, tags);

        // Act
        String postDTOString = postDTO.toString();

        // Assert
        assertNotNull(postDTOString);
        String expectedString = "PostDTO{id=1, title='Test Post', content='This is a test post', tagLabels=[Java]}";
        assertEquals(expectedString, postDTOString);
    }
}
