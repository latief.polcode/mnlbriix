package org.acme.repository;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.acme.model.Tag;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
public class TagRepositoryTest {

    @Inject
    TagRepository tagRepository;

    private Tag tag;

    @BeforeEach
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    void setUp() {
        String uniqueLabel = "Test" + UUID.randomUUID();
        tag = new Tag(uniqueLabel);
        tagRepository.persistAndFlush(tag);
    }

    @Test
    @Transactional
    public void testFindByLabel() {
        Optional<Tag> foundTagOpt = tagRepository.findByLabel(tag.getLabel());
        assertTrue(foundTagOpt.isPresent(), "Tag should be found with label");

        Tag foundTag = foundTagOpt.get();
        assertEquals(tag.getLabel(), foundTag.getLabel(), "The found tag label should match the expected label");
    }

    @Test
    @Transactional
    public void testFindByLabelNotFound() {
        Optional<Tag> foundTagOpt = tagRepository.findByLabel("NonExistentLabel");
        assertFalse(foundTagOpt.isPresent(), "Tag should not be found with a non-existent label");
    }
}
