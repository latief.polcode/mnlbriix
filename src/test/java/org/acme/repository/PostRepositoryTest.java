package org.acme.repository;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.acme.model.Post;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
public class PostRepositoryTest {

    @Inject
    PostRepository postRepository;

    private Post post;

    @BeforeEach
    @Transactional
    void setUp() {
        post = new Post("Halo I am the title", "This is a test post content");
        postRepository.persist(post);
    }

    @Test
    @Transactional
    public void testFindById() {
        Post foundPost = postRepository.findById(post.getId());
        assertNotNull(foundPost);
        assertEquals(post.getTitle(), foundPost.getTitle());
    }
}
