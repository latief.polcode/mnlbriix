package org.acme.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.*;

class TagTest {

    private Tag tag;

    @BeforeEach
    void setUp() {
        tag = new Tag("Java");
    }

    @Test
    void testTagConstructorAndGetters() {
        assertEquals("Java", tag.getLabel());
    }

    @Test
    void testTagSetters() {
        tag.setLabel("Quarkus");

        assertEquals("Quarkus", tag.getLabel());
    }

    @Test
    void testPostsAssociation() {
        Post post1 = new Post("Title1", "Content1");
        Post post2 = new Post("Title2", "Content2");

        assertTrue(tag.getPosts().isEmpty());

        tag.getPosts().add(post1);
        tag.getPosts().add(post2);

        assertEquals(2, tag.getPosts().size());
        assertTrue(tag.getPosts().contains(post1));
        assertTrue(tag.getPosts().contains(post2));
    }
}
