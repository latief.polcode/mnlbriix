package org.acme.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.*;

class PostTest {

    private Post post;

    @BeforeEach
    void setUp() {
        post = new Post("Sample Title", "Sample Content");
    }

    @Test
    void testPostConstructorAndGetters() {
        assertEquals("Sample Title", post.getTitle());
        assertEquals("Sample Content", post.getContent());
    }

    @Test
    void testPostSetters() {
        post.setTitle("New Title");
        post.setContent("New Content");

        assertEquals("New Title", post.getTitle());
        assertEquals("New Content", post.getContent());
    }

    @Test
    void testAddAndRemoveTags() {
        Tag tag1 = new Tag("Tag1");
        Tag tag2 = new Tag("Tag2");

        assertTrue(post.getTags().isEmpty());

        post.addTag(tag1);
        post.addTag(tag2);

        assertEquals(2, post.getTags().size());
        assertTrue(post.getTags().contains(tag1));
        assertTrue(post.getTags().contains(tag2));

        post.removeTag(tag1);

        assertEquals(1, post.getTags().size());
        assertFalse(post.getTags().contains(tag1));
        assertTrue(post.getTags().contains(tag2));
    }
}
